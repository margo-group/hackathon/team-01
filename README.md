# Team-01

# Welcome to Unicef BlockChain Web App 0.1 : Assistant in managing the records for under priviledged children 


### Prerequisites

What things you need to download and install the software and how to install them
```
.1. Browser extension : Metamax Extension
2. Nodejs server
3. Ganache Personal Block chain
4. Truffle Framework for Etherium Blockchain
```

## Running the tests

```
.truffle test
```

## Built With

```
.* Javascript, HTML5, CSS3
* Solidity - For smart contracts
```
## Versioning

```
.Truffle v5.0.2 (core: 5.0.2)
Solidity v0.5.0 (solc-js)
Node v8.10.0
```


## Authors and Contributor

* **Tejas Bhor - Sudhanshu Chaudhari - Clement - Murali Krishna - Arjun Singh** - *Initial work*





